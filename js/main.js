
$(document).ready(function() {
    //methods
    $("#verified").hide();
    $("#not-verified").hide();
    console.log( 'uuid ',getUrlParameter('id') );
    userStatus(getUrlParameter('id'));
});
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function userStatus(id){
    console.log( 'id',id );
        var data = "uuid=" + id;
        $.ajax({
            url: 'http://52.221.4.121:8081/dlt/getAssetById',
            dataType: 'json',
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            data: data,
            success: function (data, textStatus, jQxhr) {
                console.log(data);
                toastr.remove();
                if (data.success) {
                    toastr.success('User is verified', '', {positionClass: 'toast-bottom-right'});
                    $("#name").text(data.result.name);
                    $("#phone").text(data.result.phoneno);
                    data.result.healthstatus?$("#status").text('GOOD'):'POOR';
                    $('.img-div').css('background-image', 'url(' + data.result.link + ')');
                    $("#not-verified").hide();
                    $("#verified").show();
                }
                else {
                    toastr.error(data.error, '', {positionClass: 'toast-bottom-right'});
                    $("#not-verified").show();
                    $("#verified").hide();
                }

            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
                toastr.error('User is not verified', '', {positionClass: 'toast-bottom-right'});
                $("#not-verified").show();
                $("#verified").hide();
            }
        });
}
